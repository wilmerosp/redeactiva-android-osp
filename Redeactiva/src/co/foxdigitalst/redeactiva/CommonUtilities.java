package co.foxdigitalst.redeactiva;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;

/**
 * Created by OSP on 20/04/2016.
 */
public class CommonUtilities {
    // give your server registration url here
    static final String SERVER_URL = Resources.getSystem().getString(R.string.ur_rutinas)+"/cron/register.php";

    // Google project id
    static final String SENDER_ID = Resources.getSystem().getString(R.string.google_app_id);

    /**
     * Tag used on log messages.
     */
    static final String TAG = "AndroidHive GCM";

    static final String DISPLAY_MESSAGE_ACTION =
            "co.foxdigitalst.redeactiva.DISPLAY_MESSAGE";

    static final String EXTRA_MESSAGE = "message";

    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
}
