package co.foxdigitalst.redeactiva;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

//import com.android.volley.toolbox.NetworkImageView;
//import com.android.volley.toolbox.Volley;
//import com.parse.ParseNotificationManager;

public class HistoriasListAdapter extends BaseAdapter {

	private ArrayList<Historia> historias;
	private Activity activity;
	public ImageLoader imageLoader;
	//private static Singleton singleton = new ParseNotificationManager.Singleton( );

	public HistoriasListAdapter(Activity act, ArrayList<Historia> r) {
		this.activity = act;
		imageLoader = new ImageLoader(act);
		this.historias = r;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return this.historias.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null){ 
			LayoutInflater inflater = (LayoutInflater) activity 
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.lista_historias, parent, false);
			holder = new ViewHolder();

			convertView.setTag(holder);
		}else{ 
			holder = (ViewHolder) convertView.getTag();
		}


		holder.txt_nombre = (TextView) convertView.findViewById(R.id.txt_usuario);
		holder.txt_deporte = (TextView) convertView.findViewById(R.id.txt_deporte);
		holder.txt_descripcion = (TextView) convertView.findViewById(R.id.txt_historia);
		holder.mImageView = (ImageView) convertView.findViewById(R.id.imghistoria);
		Historia r = historias.get(position);

		//String[] hora_temp = r.getHora().split(" ");
		//String[] hora_temp1 = hora_temp[1].split(":");
		//int h = Integer.parseInt( hora_temp1[0] );
		//int min = Integer.parseInt( hora_temp1[1] );
		//String siglas = "a.m";
		
		/*if( h> 12 ){
			h = h-12;
			siglas = "p.m";
		}
		*/

		holder.txt_nombre.setText(r.getNombreUsuario());
		holder.txt_deporte.setText(r.getDeporte());
		holder.txt_descripcion.setText(r.getDescripcion());
		imageLoader.DisplayImage(r.getAdjunto(), holder.mImageView);
		Log.i("URL:", r.getAdjunto());
	/*
		holder.mImageloader = MySingleton.getInstance(this.historias).getImageLoader();
		holder.mImageloader.get(R.string.ur_rutinas + "/media/uploads/" + r.getAdjunto(),
				holder.ImageLoader.getImageListener(holder.mImageView,
				R.drawable.def_image, R.drawable.err_image));
	*/
		return convertView;
	}
	
	static class ViewHolder {
		TextView txt_nombre, txt_descripcion, txt_deporte;
		//ImageLoader imageloader;
		ImageView mImageView;
	}

}
