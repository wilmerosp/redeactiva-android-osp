package co.foxdigitalst.redeactiva;

public class Historia {
	private String objectId;
	private String deporte;
	private String descripcion;
	private String adjunto;
	private String idUsuario;

	public String getDeporte() {
		return deporte;
	}

	public void setDeporte(String deporte) {
		this.deporte = deporte;
	}

	private String nombreUsuario;

	public Historia(String objectId, String descripcion, String adjunto,
			String idUsuario, String nombreUsuario, String deporte) {
		super();
		this.objectId = objectId;
		this.descripcion = descripcion;
		this.adjunto = adjunto;
		this.idUsuario = idUsuario;
		this.nombreUsuario = nombreUsuario;
		this.deporte = deporte;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getAdjunto() {
		return adjunto;
	}

	public void setAdjunto(String adjunto) {
		this.adjunto = adjunto;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
}
