package co.foxdigitalst.redeactiva;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class Recordatorio extends Service {
    public Recordatorio() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(getString(R.string.app_name), "Servicio notificaciones iniciado.");
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
