package co.foxdigitalst.redeactiva;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Perfil extends Activity {

	Usuario usuario;
	int id;
	RadioGroup sexo;
	RadioButton smas,sfem,sind;
	TextView txt_email;
	EditText nombre,apellido,email,telefono,frase,pass, c_pass;
	ImageView foto;
	Bitmap imagenFoto;
	String imageHttpAddress;
	String idgenero;
	CheckBox editaPassword;
	View rootView;
	Button changeImg;
	private String fotoName="";
	private Bitmap captura;
	private Boolean ePass = false;
	static final int REQUEST_IMAGE_CAPTURE = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_perfil);
		
		Intent intent = getIntent();
		id = intent.getIntExtra("id_user", 0);	
		sexo = (RadioGroup)findViewById(R.id.pefil_sexo);
		smas = (RadioButton)findViewById(R.id.rad_masc);
		sfem = (RadioButton)findViewById(R.id.rad_fem);
		sind = (RadioButton)findViewById(R.id.rad_nes);
		txt_email = (TextView)findViewById(R.id.txt_email);
		pass = (EditText)findViewById(R.id.edt_pass);
		c_pass = (EditText)findViewById(R.id.edt_confirmar_pass);
		nombre = (EditText)findViewById(R.id.perfil_nombre);
		apellido = (EditText)findViewById(R.id.perfil_apellido);
		email = (EditText)findViewById(R.id.perfil_email);
		telefono = (EditText)findViewById(R.id.perfil_telefono);
		frase = (EditText) findViewById(R.id.perfil_frase);
		foto = (ImageView) findViewById(R.id.perfil_foto);
		editaPassword = (CheckBox) findViewById(R.id.edit_pass_sino);
		changeImg = (Button) findViewById(R.id.profile_changeimg);
		if(id>0){
			new ObtenerPerfil().execute();
		}

		smas.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(smas.isChecked()){
					idgenero = "1";
				}
			}
		});

		sfem.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(sfem.isChecked()){
					idgenero = "2";
				}
			}
		});

		sind.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(sind.isChecked()){
					idgenero = "3";
				}
			}
		});

		findViewById(R.id.btn_guardar).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stu
				boolean titulo_ok;

				if (!ePass) {
					titulo_ok = true;

				} else {
					titulo_ok = validarPass();
				}


				if (!titulo_ok) {
					new AlertDialog.Builder(Perfil.this)
							.setTitle("Campos Obligatorios")
							.setMessage("Verifique que su nueva contraseña no está vacia y confirmela. Vuelva a intentarlo..")
							.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									// continue with delete
									dialog.cancel();
								}
							})
							.setIcon(android.R.drawable.ic_dialog_alert)
							.show();

				} else {
					String name = guardarBitmap();
					new GuardarPerfil().execute(name);
				}

			}
		});
		
		findViewById(R.id.btn_salir).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				salir();
			}
		});

		editaPassword.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (editaPassword.isChecked()) {
					ePass = Boolean.TRUE;
				} else {
					ePass = Boolean.FALSE;
				}

			}
		});

		editaPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
										 boolean isChecked) {
				if (isChecked) {
					pass.setEnabled(true);
					c_pass.setEnabled(true);
				} else {
					pass.setEnabled(false);
					c_pass.setEnabled(false);
				}
			}
		});

		changeImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dispatchTakePictureIntent();
			}
		});
	}

	private void dispatchTakePictureIntent() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
		}
	}

	protected void salir() {
		// TODO Auto-generated method stub
		configuracionDB admin = new configuracionDB(Perfil.this,
	            "redeactiva", null, 1);
	    SQLiteDatabase bd = admin.getWritableDatabase();        
	    int cant = bd.delete("user_redeactiva", "codigo=1", null);
	    bd.close();
	    
	    if (cant == 1)
	    	Log.d(getString(R.string.app_name), "Se borró Email satisfactoriamente");
	    else
	    	Log.d(getString(R.string.app_name), "No existia");
		
		Intent i = new Intent(Perfil.this, MainActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    startActivity(i);
	    finish();
	}
	
	protected boolean validarPass() {
		// TODO Auto-generated method stub
		boolean p_ok = false;
		
		if(!TextUtils.isEmpty(pass.getText().toString().trim()) && 
			pass.getText().toString().trim().equals( c_pass.getText().toString().trim() ) ){
			p_ok = true;
		}
		return p_ok;
	}


	private void obtenerPerfil() {
		
		httpHandler handler = new httpHandler();
		String r = handler.get( getString( R.string.ur_servicio ) + "/login/perfil/" + id );
		
		Log.d( getString( R.string.app_name ), "respuesta: " + r );
		
		try {
			JSONObject json = new JSONObject(r);
			//usuario = new Usuario(json.getString("email"), "");
			usuario = new Usuario();
			usuario.setEmail(json.getString("email"));
			usuario.setNombre(json.getString("nombres"));
			usuario.setApellido(json.getString("apellidos"));
			usuario.setTelefono(json.getString("telefono"));
			usuario.setFrase(json.getString("frase"));
			usuario.setFoto(json.getString("foto"));
			usuario.setIdgenero(Integer.parseInt(json.getString("genero")));
			//usuario.setFrase(json.getString("frase"));
			if(usuario.getFoto().isEmpty()){
				imagenFoto = null;
			}else {
				imageHttpAddress = getString(R.string.ur_rutinas) + "/media/uploads/" + usuario.getFoto();
				URL imageURL = new URL(imageHttpAddress);
				HttpURLConnection connection = (HttpURLConnection) imageURL.openConnection();
				connection.connect();
				imagenFoto = BitmapFactory.decodeStream(connection.getInputStream());
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public class ObtenerPerfil extends AsyncTask<Void, Void, Void>{		

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub			
			try{
				obtenerPerfil();
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {
				//Uri imageURI = Uri.parse(getString(R.string.ur_rutinas) + "/media/uploads/" + usuario.getFoto());
				txt_email.setText(getString(R.string.str_email_perfil) + usuario.getEmail());
				nombre.setText(usuario.getNombre());
				apellido.setText(usuario.getApellido());
				email.setText(usuario.getEmail());
				telefono.setText(usuario.getTelefono());
				frase.setText((usuario.getFrase()));
				if(usuario.getIdgenero()==1){
					sexo.check(R.id.rad_masc);
					idgenero = "1";
				}else if(usuario.getIdgenero()==2){
					sexo.check(R.id.rad_fem);
					idgenero = "2";
				}else if(usuario.getIdgenero()==3){
					sexo.check(R.id.rad_nes);
					idgenero = "3";
				}
				if (imagenFoto != null){
					foto.setImageBitmap(imagenFoto);
				}
			} catch (Exception e) {
				// TODO: handle exception
				Toast.makeText(getApplicationContext(), "Error cargando la imagen: "+e.getMessage(), Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
			
			findViewById(R.id.loading).setVisibility(View.GONE);
			findViewById(R.id.ly_perfil).setVisibility(View.VISIBLE);
 		}
		
	}
	
	class GuardarPerfil extends AsyncTask<String, Void, Boolean>{

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			findViewById(R.id.loading).setVisibility(View.VISIBLE);
			findViewById(R.id.ly_perfil).setVisibility(View.GONE);
		}

		@Override
		protected Boolean doInBackground(String... params) {
			// TODO Auto-generated method stub
			try{
				return guardarPerfil(params[0]);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {
				
				new AlertDialog.Builder( Perfil.this )
			    .setTitle("Muy bien!")
			    .setMessage("Su Perfil se ha guardado con Éxito.")
			    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			            // continue with delete
			        	dialog.cancel();
			        }
			     })				    
			    .setIcon(android.R.drawable.ic_dialog_alert)
			     .show();
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace(); 
			}
			
			findViewById(R.id.loading).setVisibility(View.GONE);
			findViewById(R.id.ly_perfil).setVisibility(View.VISIBLE);
		}
		
	}



	public boolean guardarPerfil(String filename){

		try {
			FileInputStream fis = null;
			if(!filename.equals("null")) {
				fis = new FileInputStream(filename);
			}

			String urlUpload = "";

			// TODO Auto-generated method stub
			try {

				List<NameValuePair> data = new ArrayList<NameValuePair>();
				data.add(new BasicNameValuePair("accion", "guardarPerfil"));
				data.add(new BasicNameValuePair("id_user", id + ""));
				data.add(new BasicNameValuePair("password", pass.getText().toString()));
				data.add(new BasicNameValuePair("email", email.getText().toString()));
				data.add(new BasicNameValuePair("nombres", nombre.getText().toString()));
				data.add(new BasicNameValuePair("apellidos", apellido.getText().toString()));
				if(telefono.getText().toString().equals("") || telefono.getText().toString().equals("0")){
					data.add(new BasicNameValuePair("telefono", "000"));
				}else {
					data.add(new BasicNameValuePair("telefono", telefono.getText().toString()));
				}
				data.add(new BasicNameValuePair("frase", frase.getText().toString()));
				data.add(new BasicNameValuePair("idgenero", idgenero));
				data.add(new BasicNameValuePair("foto",fotoName));

				urlUpload = getString(R.string.ur_servicio) + "/perfil/" + id + "/nueva/" + fotoName;

				httpHandler handler = new httpHandler();
				String r = handler.post(getString(R.string.ur_servicio) + "/login/perfil/actualizar/" + ePass, data);

				Log.d(getString(R.string.app_name), "respuesta: " + r);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if(!filename.equals("null")) {
					Log.d("ArtCreative", "url upload: " + urlUpload);
					HttpFileUploader htfu = new HttpFileUploader(urlUpload, "noparamshere", filename);
					htfu.doStart(fis);
				}
			}catch (Exception e){
				Log.e(getString(R.string.app_name), "No se cargó la imagen.");
			}

		}catch (FileNotFoundException e){
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
			Bundle extras = data.getExtras();
			Bitmap imageBitmap = (Bitmap) extras.get("data");

			int screenWidth = 768;
			int screenHeight = 768;

			Bitmap scaled = Bitmap.createScaledBitmap(imageBitmap, screenWidth,screenHeight , true);
			imageBitmap=scaled;

			captura = imageBitmap;
			foto.setVisibility(View.VISIBLE);
			foto.setImageBitmap(captura);

			//findViewById(R.id.btn_guardar).setEnabled(true);
		}
	}


	//@SuppressLint("SimpleDateFormat")
	public String guardarBitmap(){

		//Date fecha = new Date();
		//SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");

		//String fechaHora = format.format(fecha);

		File imagesFolder = new File(Environment.getExternalStorageDirectory(), "redeactiva");
		if(!imagesFolder.exists())
			imagesFolder.mkdirs();
		String n = "fotop_" + id + ".jpg";
		fotoName = n;
		File pictureFile = new File(imagesFolder, n );

		if(captura == null)
			return "null";

		try {
			FileOutputStream fos = new FileOutputStream(pictureFile);
			captura.compress(Bitmap.CompressFormat.JPEG, 85, fos);
			fos.flush();
			fos.close();
		} catch (FileNotFoundException e) {
			Log.d("No File", "File not found: " + e.getMessage());
		} catch (IOException e) {
			//Log.d(TAG, "Error accessing file: " + e.getMessage());
		}

		//Toast.makeText(getApplicationContext(), "Se ha guardado su foto correctamente. ahora la puedes compartir..", Toast.LENGTH_LONG).show();
		String t = Environment.getExternalStorageDirectory() + "/redeactiva/"+n;
		return t;
		//new GuardarPerfil().execute(t);
	}

}
