package co.foxdigitalst.redeactiva;

public class Usuario {
	private String foto ,nombre, apellido, telefono, frase, email, pass;
	private int idgenero;

	public Usuario(){
		super();
	}


	public Usuario(String email, String pass) {
		super();
		this.email = email;
		this.pass = pass;
	}

	public void setUsuario(String foto,
						   String nombre,
						   String apellido,
						   String telefono,
						   String frase,
						   String email,
						   String pass,
							int idgenero){
		this.foto = foto;
		this.nombre = nombre;
		this.apellido = apellido;
		this.telefono = telefono;
		this.frase = frase;
		this.email = email;
		this.pass = pass;
		this.idgenero = idgenero;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getFrase() {
		return frase;
	}
	public void setFrase(String frase) {
		this.frase = frase;
	}
	public String getApellido() {return apellido;}
	public void setApellido(String apellido) {this.apellido = apellido;}
	public int getIdgenero() {return idgenero;}
	public void setIdgenero(int idgenero) {this.idgenero = idgenero;}
}
