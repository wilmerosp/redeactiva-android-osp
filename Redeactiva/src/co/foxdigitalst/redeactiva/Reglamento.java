package co.foxdigitalst.redeactiva;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Reglamento extends Activity {

	WebView wv_reglamento;
	boolean loadingFinished = true;
	boolean redirect = false;
	String url;
	@SuppressLint("SetJavaScriptEnabled")
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_reglamento);

		Intent intent = getIntent();
		url = intent.getStringExtra("url");

		WebView webView=new WebView(Reglamento.this);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setPluginState(PluginState.ON);

		webView.setWebViewClient(new Callback());
		webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + url);
		setContentView(webView);
	}

	private class Callback extends WebViewClient {
	@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			return(false);
		}

		/*WebView myWebView = (WebView) findViewById(R.id.wv_reglamento);
		myWebView.getSettings().setJavaScriptEnabled(true);
		myWebView.getSettings().setPluginState(PluginState.ON);
		myWebView.loadUrl("https://docs.google.com/gview?embedded=true&url="+"https://www.paralympic.org/sites/default/files/document/140211182255650_2014_02_11+IPC+Athletics+Rio+Medal+Event+Replacement+Policy.pdf");
		setContentView(myWebView);
		*/

		/*
		Log.d("redeactiva", "url reglamento: "+url);
		
		wv_reglamento = (WebView) findViewById(R.id.wv_reglamento);

		wv_reglamento.getSettings().setJavaScriptEnabled(true);
		wv_reglamento.getSettings().setPluginState(PluginState.ON);
        wv_reglamento.getSettings().setAllowFileAccess(true);
		wv_reglamento.setWebViewClient(new WebViewClient() {
				
			@Override
	        public boolean shouldOverrideUrlLoading(WebView view, String url) {
				
	            return (false);
	        }
			
			@Override
			public void onPageFinished(WebView view, String url) {
			        // do your stuff here
				   wv_reglamento.setVisibility(View.VISIBLE);
		    	   findViewById(R.id.loading).setVisibility(View.GONE);
			}
		});
		//wv_reglamento.loadUrl("http://docs.google.com/gview?embedded=true&url=" + url);
		wv_reglamento.loadDataWithBaseURL("http://docs.google.com/gview?embedded=true&url=" + url, HTML_DATA, null, "UTF-8",null);
		//setContentView(wv_reglamento);
		*/
	}

}
