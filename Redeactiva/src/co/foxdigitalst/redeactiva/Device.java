package co.foxdigitalst.redeactiva;

/**
 * Created by OSP on 02/05/2016.
 */
public class Device {
    public String deviceId = "";
    public String senderId = "";

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public void device(){

    }
}
